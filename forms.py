# coding:utf-8

from flask_wtf import FlaskForm
from flask import session

from wtforms import StringField, PasswordField, SubmitField, SelectField, FileField, TextAreaField

from wtforms.validators import DataRequired,EqualTo,ValidationError
#导入数据验证方法
from  models import User

'''
表单文件：
登录表单：
1.账号
2.密码
3.登录按钮你
'''


class LoginForm(FlaskForm):
    name = StringField(
        label=u"账号",
        validators=[
            DataRequired(u"账号不能为空")
        ],
        description=u"账号",
        render_kw={
            "class": "form-control",
            "placeholder": u"请输入账号！"
        }
    )
    pwd = PasswordField(
        label=u"密码",
        validators=[
            DataRequired(u"密码不能为空")
        ],
        description="密码",
        render_kw={
            "class": "form-control",
            "placeholder": u"请输入密码！"

        }
    )

    submit = SubmitField(
        u"登录",
        render_kw={
            "class": "btn btn-primary"
        }
    )

    def validate_pwd(self,field):
        pwd=field.data
        user=User.query.filter_by(name=self.name.data).first()
        if not user.check_pwd(pwd):
            raise ValidationError(u"密码不正确")


'''注册表单

1.账号
2.密码
3.确认密码
4.验证码
5.注册按钮
'''


class RegisterForm(FlaskForm):
    name = StringField(
        label=u"账号",
        validators=[
            DataRequired(u"账号不能为空！")
        ],
        description=u"账号",
        render_kw={
            "class": "form-control",
            "placeholder": u"请输入账号！"
        }
    )
    pwd = PasswordField(
        label=u"密码",
        validators=[
            DataRequired(u"密码不能为空！")
        ],
        description="密码",
        render_kw={
            "class": "form-control",
            "placeholder": u"请输入密码！"

        }
    )

    repwd = PasswordField(
        label=u"确认密码",
        validators=[
            DataRequired(u"确认密码不能为空！"),
            EqualTo('pwd',message=u"两次输入密码不一致")
        ],
        description="确认密码",
        render_kw={
            "class": "form-control",
            "placeholder": u"请输入确认密码！"

        }
    )
    code = StringField(
        label=u"验证码",
        validators=[
            DataRequired(u"验证码不能为空！")
        ],
        description=u"验证码",
        render_kw={
            "class": "form-control",
            "placeholder": u"请输入验证码！"
        }
    )

    submit = SubmitField(
        u"注册",
        render_kw={
            "class": "btn btn-success"  # 样式success为绿色
        }
    )

    #（验证账号唯一性，固定格式）自定义字段验证规则：validate_字段名
    def validate_name(self,field):
        name=field.data
        user =User.query.filter_by(name=name).count()
        if user > 0:
            raise ValidationError(u"账号已存在，不能重复注册")

    #自定义验证码验证功能
    def validate_code(self,field):
        code=field.data
        # if not session.has_key("code")
        #     raise ValidationError(u"没有验证码")

        if session["code"].lower() !=code.lower():
            raise ValidationError(u"验证码错误")


"""
发布文章表单
1.标题
2.分类
3.封面
4.内容
"""


class ArtForm(FlaskForm):
    title = StringField(
        label=u"标题",
        description=u"标题",
        validators=[],
        render_kw={
            "class": "form-control",
            "placeholder": u"请输入标题"
        }
    )
    cate = SelectField(
        label="分类",
        description="分类",
        validators=[],
        choices=[(1, "IT"), (2, "日报"), (3, "项目")],
        default=2,  # 默认
        coerce=int,  # 定义整型
        render_kw={
            "class": "form-control"
        }
    )
    logo = FileField(
        label=u"封面",
        validators=[],
        description=u"封面",
        render_kw={
            "class": "form-control-file"
        }
    )
    content = TextAreaField(
        label=u"内容",
        validators=[],
        description=u"内容",
        render_kw={
            "style":"height:300px; width:800px;",
            "id": "content"
        }

    )
    submit=SubmitField(
        u"发布文章",
        render_kw={
            "class":"btn btn-primary"
        }
    )
