# coding:utf-8

import datetime
import os
from flask import Flask, render_template, redirect, flash
from flask import session, Response  # 引入保存会话和响应的方法
from flask import url_for,request #引入生成路由方法urlfor
from flask_sqlalchemy import SQLAlchemy
import pymysql


from forms import LoginForm, RegisterForm, ArtForm

from models import User, db, app  # 引入数据模型方法
from werkzeug.security import generate_password_hash  # 引入加密方法

from functools import wraps #引入装饰器方法

# app = Flask(__name__)

app.config["SECRET_KEY"] = "12345678"

#登录装饰器
def user_login_req(f):
    @wraps(f)
    def login_req(*args,**kwargs):
        if "user" not in session:
            return redirect(url_for('login',next=request.url))
        return f(*args,**kwargs)

    return login_req


# 登录
@app.route("/login/", methods=["GET", "POST"])
def login():
    form = LoginForm()  # 实例化
    if form.validate_on_submit():
        data = form.data
        session["user"] = data["name"]
        flash("登录成功！", "ok")
        return redirect("/art/list/")
    return render_template("login.html", title="登录", form=form)  # 渲染模板


# 注册
@app.route("/register/", methods=["GET", "POST"])
def register():
    form = RegisterForm()

    if form.validate_on_submit():
        data = form.data
        # 保存数据操作
        user = User(
            name=data["name"],
            pwd=generate_password_hash(data["pwd"]),
            addtime=datetime.datetime.now().strftime("%Y-%m-%d %H:%M:%S")

        )
        db.session.add(user)
        db.session.commit()
        # 定义一个会话闪现（弹窗），需要引入flash
        flash("注册成功，请登录！", "ok")
        return redirect("/login/")
    else:
        flash("注册失败,请重新注册!", "error")

    return render_template("register.html", title=u"注册", form=form)  # 渲染模板


# 退出(302跳转登录页面）
@app.route("/logout/", methods=["GET"])
@user_login_req
def logout():
    session.pop("user",None)
    return redirect("/login/")  # 渲染模板


# 发布文章
@app.route("/art/add/", methods=["GET", "POST"])
@user_login_req
def art_add():
    form = ArtForm()  # 实例化
    return render_template("art_add.html", title="发布文章", form=form)  # 渲染模板


# 编辑文章
@app.route("/art/edit/<int:id>/", methods=["GET", "POST"])
@user_login_req
def art_edit(id):
    return render_template("art_edit.html")  # 渲染模板


# 删除文章
@app.route("/art/del/<int:id>/", methods=["GET", "POST"])
@user_login_req
def art_del(id):
    return render_template("art_list.html")  # 渲染模板


# 文章列表
@app.route("/art/list/", methods=["GET"])
@user_login_req
def art_list():
    return render_template("art_list.html", title="文章列表")  # 渲染模板


# 验证码
@app.route("/codes/", methods=["GET"])
def code():
    from codes import Codes
    c = Codes()
    info = c.create_code()
    image = os.path.join(os.path.dirname(__file__), "static/code" + "/" + info["img_name"])
    with open(image, "rb") as f:
        image = f.read()
    session["code"] = info["code"]
    # print(session["code"])
    return Response(image, mimetype="jpeg")


if __name__ == '__main__':
    app.run(debug=True, host="127.0.0.1", port=8082)
